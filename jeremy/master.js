// means "when the document loads, do this stuff."

$(document).ready(function() {
	$(".RecipeList").click(function(){
		// console.log("woohoo!");
		$(".RecipeList").toggleClass("Closed");
		$(".RecipeFAB").toggleClass("Closed");
	});
	$(".BackButton").click(function(){
		$(".RecipeList").toggleClass("Closed");
		$(".RecipeFAB").toggleClass("Closed");
	});

	// ANIMATIONS USING VELOCITY.JS

	$('.RecetioLogo').velocity("transition.slideDownIn", {
    delay: 0,
    duration: 500,
	});
	$('.RecipeList li').velocity("transition.slideLeftIn", {
    delay: 0,
    duration: 500,
    stagger: 50,
	});
	$('.RecipeFAB').velocity("transition.slideUpIn", {
		delay: 800,
		duration: 500,

	});
});