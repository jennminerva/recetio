#Recetio

An app to help you keep track of your recipes.

###To-do list:

- Jenn: code clean up
- Jenn: "Add Recipe" flow.
	- User taps FAB (Floating Action Button)
	- Fab animates out to the right
	- Form animates in from bottom of viewport
	- Close button or submit button animates shade down
- Jeremy: make a mock data file (JSON)

Future Functionality:

- consume data file in list
- open detail view for an individual item when list is tapped
- CRAFTCMS generate JSON?
- Create a new item form
- Edit an item
- Upload images
- Sort by tags
- Share/Publish?
- Search
- Smart search

Recipe Data Structure:

- ID
- Title
- DateCreated
- Order
- LastEdited
- Body (Markdown?)
- Thumbnail?
- Tags
- Category
