// means "when the document loads, do this stuff."

$(document).ready(function() {
	// a javascript media query of sorts
	if (document.documentElement.clientWidth < 800) {
		// user exposes a recipe's detail
		$(".RecipeList li").click(function(){
			$(".RecipeList").toggleClass("Closed");
			$(".RecipeDetail").toggleClass("Open");
		});
		// user closes a recipe
		$("#RecipeDetailClose").click(function(){
			$(".RecipeList").toggleClass("Closed");
			$(".RecipeDetail").toggleClass("Open");
		});
		// user closes/cancel Add A Recipe
		$("#RecipeAddClose").click(function(){
			$(".RecipeAdd").toggleClass("UnSeen");
		});
		// user intiates adding a recipe
		$("#RecipeAddButton").click(function(){
			$(".RecipeAdd").toggleClass("UnSeen");
		});
	}
	if (document.documentElement.clientWidth > 800) {
		// user closes/cancel Add A Recipe
		$("#RecipeAddClose").click(function(){
			$(".RecipeAdd").toggleClass("UnSeen");
		});
		// user intiates adding a recipe
		$("#RecipeAddButton").click(function(){
			$(".RecipeAdd").toggleClass("UnSeen");
		});
	}
});



